﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CLAM.Models.View.Revisions
{
    public class Create
    {
        [Required, DisplayName("Text")]
        public string Text { get; set; }

        [DisplayName("Require address")]
        public bool RequireAddress { get; set; }
    }
}
