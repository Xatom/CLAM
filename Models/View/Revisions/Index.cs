﻿using CLAM.Models.Database;
using System.Collections.Generic;

namespace CLAM.Models.View.Revisions
{
    public class Index
    {
        public List<Revision> Revisions { get; set; }
    }
}
