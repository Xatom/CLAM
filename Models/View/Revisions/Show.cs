﻿using System.ComponentModel;

namespace CLAM.Models.View.Revisions
{
    public class Show
    {
        public int ClaId { get; set; }

        public int Id { get; set; }

        [DisplayName("Revision")]
        public int Number { get; set; }

        [DisplayName("Text")]
        public string Text { get; set; }

        [DisplayName("Require address")]
        public bool RequireAddress { get; set; }
    }
}
