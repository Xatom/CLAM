﻿using CLAM.Models.Database;
using System.Collections.Generic;

namespace CLAM.Models.View.Signers
{
    public class Index
    {
        public List<Signer> Signers { get; set; }
    }
}
