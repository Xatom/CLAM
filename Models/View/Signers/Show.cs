﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CLAM.Models.View.Signers
{
    public class Show
    {
        [DataType(DataType.EmailAddress), DisplayName("Email address")]
        public string Email { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Address")]
        public string Address { get; set; }

        [DisplayName("Signed at")]
        public DateTime? SignedAt { get; set; }
    }
}
