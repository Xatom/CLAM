using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CLAM.Models.View.Account
{
    public class Register
    {
        [Required, EmailAddress, DisplayName("Email address")]
        public string Email { get; set; }

        [Required, DataType(DataType.Password), DisplayName("Password")]
        public string Password { get; set; }

        [Required, DataType(DataType.Password), DisplayName("Confirm password"), Compare(nameof(Password))]
        public string ConfirmPassword { get; set; }
    }
}
