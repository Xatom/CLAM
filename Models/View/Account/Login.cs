using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CLAM.Models.View.Account
{
    public class Login
    {
        [Required, EmailAddress, DisplayName("Email address")]
        public string Email { get; set; }

        [Required, DataType(DataType.Password), DisplayName("Password")]
        public string Password { get; set; }

        [DisplayName("Remember me")]
        public bool RememberMe { get; set; }

        [HiddenInput]
        public string ReturnUrl { get; set; }

        public bool Unverified { get; set; }
    }
}
