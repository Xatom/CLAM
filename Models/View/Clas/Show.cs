﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CLAM.Models.View.Clas
{
    public class Show
    {
        public int Id { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("API key")]
        public string ApiKey { get; set; }

        [DisplayName("Webhook secret")]
        public string Secret { get; set; }

        [DataType(DataType.Url), DisplayName("Webhook URL")]
        public string WebhookUrl { get; set; }
    }
}
