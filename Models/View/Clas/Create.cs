﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CLAM.Models.View.Clas
{
    public class Create
    {
        [Required, DisplayName("Name")]
        public string Name { get; set; }

        [Required, DisplayName("API key")]
        public string ApiKey { get; set; }
    }
}
