﻿namespace CLAM.Models.View.Shared
{
    public enum FlashType
    {
        Success,
        Warning,
        Error
    }
}
