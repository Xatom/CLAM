﻿namespace CLAM.Models.View.Shared
{
    public class Flash
    {
        public string Message { get; set; }

        public FlashType Type { get; set; }
    }
}
