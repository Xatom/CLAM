﻿namespace CLAM.Models.Services.Account
{
    public enum LoginStatus
    {
        Unverified,
        Success,
        Failed
    }
}
