﻿namespace CLAM.Models.Services.Signer
{
    public enum SignStatus
    {
        Signed,
        Unsigned,
        NonExistent
    }
}
