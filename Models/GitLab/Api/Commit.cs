﻿using Newtonsoft.Json;

namespace CLAM.Models.GitLab.Api
{
    public class Commit
    {
        [JsonProperty("author_email")]
        public string AuthorEmail { get; set; }

        [JsonProperty("author_name")]
        public string AuthorName { get; set; }
    }
}
