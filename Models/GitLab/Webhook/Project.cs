﻿using Newtonsoft.Json;

namespace CLAM.Models.GitLab.Webhook
{
    public class Project
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
