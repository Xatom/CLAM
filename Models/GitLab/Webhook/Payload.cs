﻿using Newtonsoft.Json;

namespace CLAM.Models.GitLab.Webhook
{
    public class Payload
    {
        public int ClaId { get; set; }

        [JsonProperty("project")]
        public Project Project { get; set; }

        [JsonProperty("object_attributes")]
        public ObjectAttributes ObjectAttributes { get; set; }
    }
}
