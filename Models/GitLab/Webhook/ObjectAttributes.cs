﻿using Newtonsoft.Json;

namespace CLAM.Models.GitLab.Webhook
{
    public class ObjectAttributes
    {
        [JsonProperty("iid")]
        public int Iid { get; set; }
    }
}
