using System;

namespace CLAM.Models.Database
{
    public interface IModel
    {
        int Id { get; set; }

        DateTime CreatedAt { get; set; }

        DateTime? ModifiedAt { get; set; }

        DateTime? DeletedAt { get; set; }
    }
}
