using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CLAM.Models.Database
{
    public class Signer : IModel
    {
        public int Id { get; set; }

        public int RevisionId { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        public string Address { get; set; }

        public DateTime? SignedAt { get; set; }

        [Required]
        public string Token { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public Revision Revision { get; set; }

        public List<MergeRequest> MergeRequests { get; set; }

        public static void ConfigureEntity(EntityTypeBuilder<Signer> builder)
        {
            builder.HasQueryFilter(u => !u.DeletedAt.HasValue);
        }
    }
}
