using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CLAM.Models.Database
{
    public class Revision : IModel
    {
        public int Id { get; set; }

        public int ClaId { get; set; }

        public int Number { get; set; }

        [Required]
        public string Text { get; set; }

        public bool RequireAddress { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public Cla Cla { get; set; }

        public List<Signer> Signers { get; set; }

        public static void ConfigureEntity(EntityTypeBuilder<Revision> builder)
        {
            builder.HasQueryFilter(u => !u.DeletedAt.HasValue);
        }
    }
}
