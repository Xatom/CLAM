using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CLAM.Models.Database
{
    public class UserCla : IModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int ClaId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public User User { get; set; }

        public Cla Cla { get; set; }

        public static void ConfigureEntity(EntityTypeBuilder<UserCla> builder)
        {
            builder.HasQueryFilter(u => !u.DeletedAt.HasValue);
        }
    }
}
