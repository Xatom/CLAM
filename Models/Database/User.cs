using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CLAM.Models.Database
{
    public class User : IModel
    {
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string Token { get; set; }

        public bool Verified { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public List<UserCla> UserClas { get; set; }

        public static void ConfigureEntity(EntityTypeBuilder<User> builder)
        {
            builder.HasQueryFilter(u => !u.DeletedAt.HasValue);
            builder.HasIndex(u => u.Email).IsUnique();
        }
    }
}
