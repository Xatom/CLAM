﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CLAM.Models.Database
{
    public class MergeRequest : IModel
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int MergeRequestIid { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public Signer Signer { get; set; }

        public static void ConfigureEntity(EntityTypeBuilder<MergeRequest> builder)
        {
            builder.HasQueryFilter(u => !u.DeletedAt.HasValue);
        }
    }
}
