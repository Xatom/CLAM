using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CLAM.Models.Database
{
    public class Cla : IModel
    {
        public int Id { get; set; }

        [Required]
        public string ApiKey { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Secret { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public List<UserCla> UserClas { get; set; }

        public static void ConfigureEntity(EntityTypeBuilder<Cla> builder)
        {
            builder.HasQueryFilter(u => !u.DeletedAt.HasValue);
        }
    }
}
