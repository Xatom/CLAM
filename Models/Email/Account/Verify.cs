﻿namespace CLAM.Models.Email.Account
{
    public class Verify
    {
        public string Email { get; set; }

        public string Url { get; set; }
    }
}
