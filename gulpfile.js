const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCss = require('gulp-clean-css');

gulp.task('js', cb => {
    const src = [
        'node_modules/jquery/dist/jquery.slim.js',
        'node_modules/popper.js/dist/umd/popper.js',
        'node_modules/bootstrap/dist/js/bootstrap.js'
    ];

    gulp.src(src)
        .pipe(concat('bundle.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('wwwroot'));

    cb();
});

gulp.task('css', cb => {
    const src = [
        'node_modules/bootswatch/dist/darkly/bootstrap.css'
    ];

    gulp.src(src)
        .pipe(concat('bundle.min.css'))
        .pipe(cleanCss())
        .pipe(gulp.dest('wwwroot'));

    cb();
});

gulp.task('default', gulp.parallel('js', 'css'));
