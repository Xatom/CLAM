﻿using CLAM.Extensions;
using CLAM.Models.View.Revisions;
using CLAM.Models.View.Shared;
using CLAM.Services;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CLAM.Controllers
{
    [Route("clas/{claId}/revisions")]
    public class RevisionsController : Controller
    {
        public RevisionsController(RevisionService revisionService)
        {
            RevisionService = revisionService;
        }

        public RevisionService RevisionService { get; }

        [HttpGet, ActionName("Index"), Route("")]
        public async Task<IActionResult> GetIndex(int claId)
        {
            var email = User.FindFirst(ClaimTypes.Email).Value;
            var revisions = await RevisionService.List(email, claId);

            return View(new Index
            {
                Revisions = revisions
            });
        }

        [HttpGet, ActionName("Create"), Route("create")]
        public IActionResult GetCreate()
        {
            return View();
        }

        [HttpPost, ActionName("Create"), Route("create")]
        public async Task<IActionResult> PostCreate(int claId, Create create)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var email = User.FindFirst(ClaimTypes.Email).Value;
            if (await RevisionService.Create(email, claId, create.Text, create.RequireAddress))
            {
                TempData.AddFlash("Your revision has been created", FlashType.Success);
            }
            else
            {
                TempData.AddFlash("Your revision couldn't be created", FlashType.Error);
            }

            return RedirectToAction("Index");
        }

        [HttpGet, ActionName("Show"), Route("{id}/show")]
        public async Task<IActionResult> GetShow(int claId, int id)
        {
            var email = User.FindFirst(ClaimTypes.Email).Value;
            var revision = await RevisionService.Single(email, id);

            if (revision == null)
            {
                TempData.AddFlash("The requested revision doesn't exist", FlashType.Warning);
                return RedirectToAction("Index");
            }

            return View(new Show
            {
                ClaId = claId,
                Id = id,
                Number = revision.Number,
                Text = revision.Text,
                RequireAddress = revision.RequireAddress
            });
        }
    }
}
