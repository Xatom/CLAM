using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CLAM.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet, AllowAnonymous, ActionName("Index"), Route("/")]
        public IActionResult GetIndex()
        {
            return View();
        }
    }
}
