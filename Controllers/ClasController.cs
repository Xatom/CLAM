﻿using CLAM.Extensions;
using CLAM.Models.View.Clas;
using CLAM.Models.View.Shared;
using CLAM.Services;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CLAM.Controllers
{
    [Route("clas")]
    public class ClasController : Controller
    {
        public ClasController(ClaService claService)
        {
            ClaService = claService;
        }

        private ClaService ClaService { get; }

        [HttpGet, ActionName("Index"), Route("")]
        public async Task<IActionResult> GetIndex()
        {
            var email = User.FindFirst(ClaimTypes.Email).Value;
            var clas = await ClaService.List(email);

            return View(new Index
            {
                Clas = clas
            });
        }

        [HttpGet, ActionName("Create"), Route("create")]
        public IActionResult GetCreate()
        {
            return View();
        }

        [HttpPost, ActionName("Create"), Route("create")]
        public async Task<IActionResult> PostCreate(Create create)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var email = User.FindFirst(ClaimTypes.Email).Value;
            if (await ClaService.Create(email, create.Name, create.ApiKey))
            {
                TempData.AddFlash("Your CLA has been created", FlashType.Success);
            }
            else
            {
                TempData.AddFlash("Your CLA couldn't be created", FlashType.Error);
            }

            return RedirectToAction("Index");
        }

        [HttpGet, ActionName("Show"), Route("{id}/show")]
        public async Task<IActionResult> GetShow(int id)
        {
            var email = User.FindFirst(ClaimTypes.Email).Value;
            var cla = await ClaService.Single(email, id);

            if (cla == null)
            {
                TempData.AddFlash("The requested CLA doesn't exist", FlashType.Warning);
                return RedirectToAction("Index");
            }

            return View(new Show
            {
                Id = cla.Id,
                Name = cla.Name,
                ApiKey = cla.ApiKey,
                Secret = cla.Secret,
                WebhookUrl = Url.Action("Webhook", "GitLab", new { claId = cla.Id }, HttpContext.Request.Scheme)
            });
        }

        [HttpGet, ActionName("Edit"), Route("{id}/edit")]
        public async Task<IActionResult> GetEdit(int id)
        {
            var email = User.FindFirst(ClaimTypes.Email).Value;
            var cla = await ClaService.Single(email, id);

            if (cla == null)
            {
                TempData.AddFlash("The requested CLA doesn't exist", FlashType.Warning);
                return RedirectToAction("Index");
            }

            return View(new Edit
            {
                Name = cla.Name,
                ApiKey = cla.ApiKey
            });
        }

        [HttpPost, ActionName("Edit"), Route("{id}/edit")]
        public async Task<IActionResult> PostEdit(int id, Edit edit)
        {
            if (!ModelState.IsValid)
            {
                return View(edit);
            }

            var email = User.FindFirst(ClaimTypes.Email).Value;
            var success = await ClaService.Edit(email, id, edit.Name, edit.ApiKey);

            if (!success)
            {
                TempData.AddFlash("The requested CLA doesn't exist", FlashType.Warning);
            }
            else
            {
                TempData.AddFlash("The CLA has been updated", FlashType.Success);
            }

            return RedirectToAction("Index");
        }
    }
}
