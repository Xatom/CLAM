﻿using CLAM.Models.GitLab.Webhook;
using CLAM.Services;
using CLAM.Services.Background;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace CLAM.Controllers
{
    [ApiController, AllowAnonymous, IgnoreAntiforgeryToken, Route("gitlab")]
    public class GitLabController : Controller
    {
        public GitLabController(GitLabChannel gitLabChannel, ClaService claService)
        {
            GitLabChannel = gitLabChannel;
            ClaService = ClaService;
        }

        private GitLabChannel GitLabChannel { get; }

        private ClaService ClaService { get; set; }

        [HttpPost, ActionName("Webhook"), Route("webhook/{claId}")]
        public async Task<IActionResult> PostWebhook(int claId, [FromBody] Payload payload)
        {
            if (!Request.Headers.TryGetValue("X-Gitlab-Event", out var type) || type.LastOrDefault() != "Merge Request Hook")
            {
                return NotFound();
            }

            if (!Request.Headers.TryGetValue("X-Gitlab-Token", out var token) || token.LastOrDefault() == null)
            {
                return BadRequest();
            }

            if (!await ClaService.ValidateSecret(claId, token.LastOrDefault()))
            {
                return Unauthorized();
            }

            payload.ClaId = claId;
            await GitLabChannel.Writer.WriteAsync(payload);

            return Ok();
        }
    }
}
