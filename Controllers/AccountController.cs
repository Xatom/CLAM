using CLAM.Extensions;
using CLAM.Models.Services.Account;
using CLAM.Models.View.Account;
using CLAM.Models.View.Shared;
using CLAM.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CLAM.Controllers
{
    [Route("account")]
    public class AccountController : Controller
    {
        public AccountController(AccountService accountService)
        {
            AccountService = accountService;
        }

        private AccountService AccountService { get; }

        [HttpGet, AllowAnonymous, ActionName("Login"), Route("login")]
        public IActionResult GetLogin(string returnUrl)
        {
            return View(new Login
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost, AllowAnonymous, ActionName("Login"), Route("login")]
        public async Task<IActionResult> PostLogin(Login login)
        {
            if (!ModelState.IsValid)
            {
                return View(login);
            }

            switch (await AccountService.Login(login.Email, login.Password))
            {
                case LoginStatus.Failed:
                    ModelState.AddModelError<Login>(r => r.Email, "Invalid email and/or password");
                    return View(login);
                case LoginStatus.Unverified:
                    login.Unverified = true;
                    return View(login);
            }

            var claims = new List<Claim> {
                new Claim(ClaimTypes.Email, login.Email)
            };

            var authenticationProperties = new AuthenticationProperties
            {
                IsPersistent = login.RememberMe
            };

            var authenticationScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            var claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(claims, authenticationScheme));
            await HttpContext.SignInAsync(authenticationScheme, claimsPrincipal, authenticationProperties);
            TempData.AddFlash("You have been logged in", FlashType.Success);

            if (Url.IsLocalUrl(login.ReturnUrl))
            {
                return Redirect(login.ReturnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet, AllowAnonymous, ActionName("Register"), Route("register")]
        public IActionResult GetRegister()
        {
            return View();
        }

        [HttpPost, AllowAnonymous, ActionName("Register"), Route("register")]
        public async Task<IActionResult> PostRegister(Register register)
        {
            if (!ModelState.IsValid)
            {
                return View(register);
            }

            if (await AccountService.Register(register.Email, register.Password))
            {
                await AccountService.SendVerifyEmail(register.Email, t => Url.Action("Verify", "Account", new { token = t }, HttpContext.Request.Scheme));
                TempData.AddFlash("Your account has been registered", FlashType.Success);
                return RedirectToAction("Login");
            }

            ModelState.AddModelError<Register>(r => r.Email, "Email address is already in use");
            return View(register);
        }

        [HttpGet, AllowAnonymous, ActionName("VerifyResend"), Route("verify/resend/{email}")]
        public async Task<IActionResult> GetVerifyResend(string email)
        {
            if (await AccountService.SendVerifyEmail(email, t => Url.Action("Verify", "Account", new { token = t }, HttpContext.Request.Scheme)))
            {
                TempData.AddFlash("A verification email has been sent", FlashType.Success);
            }
            else
            {
                TempData.AddFlash("A verification email could not be sent", FlashType.Error);
            }

            return RedirectToAction("Login");
        }

        [HttpGet, AllowAnonymous, ActionName("Verify"), Route("verify/{token}")]
        public async Task<IActionResult> GetVerify(string token)
        {
            if (await AccountService.Verify(token))
            {
                TempData.AddFlash("Your account has been verified", FlashType.Success);

            }
            else
            {
                TempData.AddFlash("Your account could not be verified", FlashType.Error);
            }

            return RedirectToAction("Login");
        }

        [HttpGet, ActionName("Logout"), Route("logout")]
        public async Task<IActionResult> GetLogout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            TempData.AddFlash("You have been logged out", FlashType.Success);
            return RedirectToAction("Index", "Home");
        }
    }
}
