﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CLAM.Controllers
{
    /// <remarks>
    /// The IgnoreAntiforgeryToken is required to show an error page even if the CSRF token is missing.
    /// </remarks>
    [AllowAnonymous, IgnoreAntiforgeryToken, Route("error")]
    public class ErrorController : Controller
    {
        [ActionName("400"), Route("400")]
        public IActionResult Get400()
        {
            return View();
        }

        [ActionName("404"), Route("404")]
        public IActionResult Get404()
        {
            return View();
        }

        [ActionName("500"), Route("500")]
        public IActionResult Get500()
        {
            return View();
        }
    }
}
