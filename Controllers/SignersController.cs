﻿using CLAM.Extensions;
using CLAM.Models.View.Shared;
using CLAM.Models.View.Signers;
using CLAM.Services;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CLAM.Controllers
{
    [Route("clas/{claId}/revisions/{revisionId}/signers")]
    public class SignersController : Controller
    {
        public SignersController(SignerService signerService)
        {
            SignerService = signerService;
        }

        private SignerService SignerService { get; }

        [HttpGet, ActionName("Index"), Route("")]
        public async Task<IActionResult> GetIndex(int revisionId)
        {
            var email = User.FindFirst(ClaimTypes.Email).Value;
            var signers = await SignerService.List(email, revisionId);

            return View(new Index
            {
                Signers = signers
            });
        }

        [HttpGet, ActionName("Show"), Route("{id}/show")]
        public async Task<IActionResult> GetShow(int id)
        {
            var email = User.FindFirst(ClaimTypes.Email).Value;
            var signer = await SignerService.Single(email, id);

            if (signer == null)
            {
                TempData.AddFlash("The requested signer doesn't exist", FlashType.Warning);
                return RedirectToAction("Index");
            }

            return View(new Show
            {
                Email = signer.Email,
                Name = signer.Name,
                Address = signer.Address,
                SignedAt = signer.SignedAt
            });
        }
    }
}
