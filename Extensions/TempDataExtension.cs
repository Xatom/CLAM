﻿using CLAM.Models.View.Shared;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CLAM.Extensions
{
    public static class TempDataExtension
    {
        private const string FlashesKey = "Flashes";

        public static void AddFlash(this ITempDataDictionary tempData, string message, FlashType type)
        {
            var flashes = tempData.GetFlashes();

            flashes.Add(new Flash
            {
                Message = message,
                Type = type
            });

            tempData[FlashesKey] = JsonConvert.SerializeObject(flashes);
        }

        public static List<Flash> GetFlashes(this ITempDataDictionary tempData)
        {
            if (tempData[FlashesKey] is string json)
            {
                return JsonConvert.DeserializeObject<List<Flash>>(json);
            }

            return new List<Flash>();
        }
    }
}
