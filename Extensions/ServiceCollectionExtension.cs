﻿using CLAM.Options;
using CLAM.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RazorLight;
using System;
using System.IO;
using System.Net.Http;

namespace CLAM.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddRazorLightEngine(this IServiceCollection services)
        {
            return services.AddSingleton(CreateRazorLightEngine);
        }

        public static IHttpClientBuilder AddGitLabClient(this IServiceCollection services)
        {
            return services.AddHttpClient<GitLabService>(CreateGitLabClient);
        }

        private static RazorLightEngine CreateRazorLightEngine(IServiceProvider services)
        {
            var env = services.GetService<IHostingEnvironment>();

            return new RazorLightEngineBuilder()
                .UseFilesystemProject(Path.Join(env.ContentRootPath, "Emails"))
                .UseMemoryCachingProvider()
                .Build();
        }

        private static void CreateGitLabClient(IServiceProvider services, HttpClient client)
        {
            var options = services.GetService<IOptions<GitLabOptions>>();

            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.BaseAddress = new Uri(options.Value.ApiUrl);
        }
    }
}
