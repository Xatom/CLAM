﻿using CLAM.Data;
using CLAM.Extensions;
using CLAM.Options;
using CLAM.Services;
using CLAM.Services.Background;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CLAM
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("Database");
            services.AddDbContext<Database>(options => options.UseNpgsql(connectionString));

            services.AddScoped<AccountService>();
            services.AddScoped<RevisionService>();
            services.AddScoped<EmailService>();
            services.AddScoped<GitLabService>();
            services.AddScoped<ClaService>();
            services.AddScoped<SignerService>();

            services.AddSingleton<EmailChannel>();
            services.AddSingleton<GitLabChannel>();

            services.AddRazorLightEngine();
            services.AddGitLabClient();
            services.AddHostedService<EmailBackgroundService>();
            services.AddHostedService<GitLabBackgroundService>();

            services.Configure<GitLabOptions>(Configuration.GetSection("GitLab"));
            services.Configure<SmtpOptions>(Configuration.GetSection("Smtp"));

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(ConfigureCookieAuthentication);

            services.AddMvc(ConfigureMvc);
        }

        public void Configure(IApplicationBuilder app)
        {
            app.MigrateDatabase();
            app.ConfigureErrorHandlers();

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc();
        }

        private static void ConfigureCookieAuthentication(CookieAuthenticationOptions options)
        {
            options.LoginPath = "/account/login";
            options.LogoutPath = "/account/logout";
        }

        private static void ConfigureMvc(MvcOptions options)
        {
            var policy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();

            options.Filters.Add(new AuthorizeFilter(policy));
            options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
        }
    }
}
