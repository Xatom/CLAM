using CLAM.Models.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;

namespace CLAM.Data
{
    public sealed class Database : DbContext
    {
        public Database(DbContextOptions<Database> options) : base(options)
        {
            ChangeTracker.Tracked += OnTracked;
            ChangeTracker.StateChanged += OnStateChanged;
            ChangeTracker.AutoDetectChangesEnabled = false;
        }

        public DbSet<Revision> Revisions { get; set; }

        public DbSet<Cla> Clas { get; set; }

        public DbSet<MergeRequest> MergeRequests { get; set; }

        public DbSet<Signer> Signers { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserCla> UserClas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Revision.ConfigureEntity(modelBuilder.Entity<Revision>());
            MergeRequest.ConfigureEntity(modelBuilder.Entity<MergeRequest>());
            Cla.ConfigureEntity(modelBuilder.Entity<Cla>());
            Signer.ConfigureEntity(modelBuilder.Entity<Signer>());
            User.ConfigureEntity(modelBuilder.Entity<User>());
            UserCla.ConfigureEntity(modelBuilder.Entity<UserCla>());
        }

        private static void OnTracked(object sender, EntityTrackedEventArgs e)
        {
            if (!e.FromQuery)
            {
                OnEntityChange(e.Entry);
            }
        }

        private static void OnStateChanged(object sender, EntityStateChangedEventArgs e)
        {
            OnEntityChange(e.Entry);
        }

        private static void OnEntityChange(EntityEntry entry)
        {
            if (!(entry.Entity is IModel entity))
            {
                return;
            }

            switch (entry.State)
            {
                case EntityState.Added:
                    entity.CreatedAt = DateTime.UtcNow;
                    break;
                case EntityState.Modified:
                    entity.ModifiedAt = DateTime.UtcNow;
                    break;
                case EntityState.Deleted:
                    entity.DeletedAt = DateTime.UtcNow;
                    entry.State = EntityState.Modified;
                    break;
            }
        }
    }
}
