FROM mcr.microsoft.com/dotnet/core/aspnet:2.2

WORKDIR /app
COPY Publish .

EXPOSE 80

ENTRYPOINT ["dotnet", "CLAM.dll"]
