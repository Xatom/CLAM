﻿using CLAM.Data;
using CLAM.Models.Database;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CLAM.Services
{
    public class RevisionService
    {
        public RevisionService(Database database, ClaService claService)
        {
            Database = database;
            ClaService = claService;
        }

        private Database Database { get; }

        private ClaService ClaService { get; }

        public async Task<List<Revision>> List(string email, int claId)
        {
            return await Database.Revisions.Where(a => a.ClaId == claId && a.Cla.UserClas.Any(u => u.User.Email == email)).ToListAsync();
        }

        public async Task<Revision> Single(string email, int id)
        {
            return await Database.Revisions.FirstOrDefaultAsync(a => a.Id == id && a.Cla.UserClas.Any(u => u.User.Email == email));
        }

        public async Task<bool> Create(string email, int claId, string text, bool requireAddress)
        {
            if (!await ClaService.Any(email, claId))
            {
                return false;
            }

            var latestRevision = Database.Revisions.Where(a => a.ClaId == claId && a.Cla.UserClas.Any(u => u.User.Email == email)).OrderByDescending(a => a.Number).FirstOrDefault();

            var revision = new Revision
            {
                ClaId = claId,
                Text = text,
                RequireAddress = requireAddress,
                Number = (latestRevision?.Number ?? 0) + 1
            };

            Database.Revisions.Add(revision);
            await Database.SaveChangesAsync();

            return true;
        }

        public async Task<Revision> Latest(int claId)
        {
            return await Database.Revisions.Where(a => a.ClaId == claId).OrderByDescending(a => a.Number).FirstOrDefaultAsync();
        }
    }
}
