using CLAM.Data;
using CLAM.Models.Database;
using CLAM.Models.Services.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CLAM.Services
{
    public class AccountService
    {
        public AccountService(Database database, IServiceProvider services, ILogger<AccountService> logger)
        {
            Database = database;
            Services = services;
            Logger = logger;
        }

        private Database Database { get; }

        private IServiceProvider Services { get; }

        private ILogger<AccountService> Logger { get; }

        public async Task<bool> Register(string email, string password)
        {
            var token = Guid.NewGuid();
            var user = new User
            {
                Email = email,
                Token = $"{token}"
            };

            var hasher = new PasswordHasher<User>();
            user.Password = hasher.HashPassword(user, password);
            Database.Users.Add(user);

            try
            {
                await Database.SaveChangesAsync();
            }
            catch (DbUpdateException exception)
            {
                Logger.LogWarning(exception, "Failed to register user");
                return false;
            }

            return true;
        }

        public async Task<bool> SendVerifyEmail(string email, Func<string, string> verifyUrlFactory)
        {
            var user = await Database.Users.FirstOrDefaultAsync(u => u.Email == email && !u.Verified);

            if (user == null)
            {
                return false;
            }

            var emailService = Services.GetService<EmailService>();
            var verifyUrl = verifyUrlFactory(user.Token);
            await emailService.Verify(user.Email, verifyUrl);

            return true;
        }

        public async Task<bool> Verify(string token)
        {
            var user = await Database.Users.FirstOrDefaultAsync(u => u.Token == token);

            if (user == null)
            {
                return false;
            }

            user.Verified = true;

            Database.Users.Update(user);
            await Database.SaveChangesAsync();
            return true;
        }

        public async Task<LoginStatus> Login(string email, string password)
        {
            var user = await Database.Users.FirstOrDefaultAsync(u => u.Email == email);

            if (user == null)
            {
                return LoginStatus.Failed;
            }

            var hasher = new PasswordHasher<User>();
            switch (hasher.VerifyHashedPassword(user, user.Password, password))
            {
                case PasswordVerificationResult.Failed:
                    return LoginStatus.Failed;
                case PasswordVerificationResult.Success:
                    return user.Verified ? LoginStatus.Success : LoginStatus.Unverified;
                case PasswordVerificationResult.SuccessRehashNeeded:
                    user.Password = hasher.HashPassword(user, password);
                    Database.Users.Update(user);
                    await Database.SaveChangesAsync();
                    goto case PasswordVerificationResult.Success;
            }

            throw new NotImplementedException("Unsupported password verification result");
        }
    }
}
