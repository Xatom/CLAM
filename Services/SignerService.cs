﻿using CLAM.Data;
using CLAM.Models.Database;
using CLAM.Models.Services.Signer;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CLAM.Services
{
    public class SignerService
    {
        public SignerService(Database database, RevisionService revisionService)
        {
            Database = database;
            RevisionService = revisionService;
        }

        private Database Database { get; }

        private RevisionService RevisionService { get; }

        public async Task<List<Signer>> List(string email, int revisionId)
        {
            return await Database.Signers.Where(s => s.RevisionId == revisionId && s.Revision.Cla.UserClas.Any(u => u.User.Email == email)).ToListAsync();
        }

        public async Task<Signer> Single(string email, int id)
        {
            return await Database.Signers.FirstOrDefaultAsync(s => s.Id == id && s.Revision.Cla.UserClas.Any(u => u.User.Email == email));
        }

        public async Task<Dictionary<string, SignStatus>> SignStatuses(int claId, ISet<string> emailAddresses)
        {
            var result = new Dictionary<string, SignStatus>();
            var emailAddressesCopy = new HashSet<string>(emailAddresses);
            var revision = await RevisionService.Latest(claId);
            var signers = await Database.Signers.Where(s => s.RevisionId == revision.Id && emailAddresses.Contains(s.Email)).ToListAsync();

            foreach (var signer in signers)
            {
                result[signer.Email] = signer.SignedAt.HasValue ? SignStatus.Signed : SignStatus.Unsigned;
                emailAddressesCopy.Remove(signer.Email);
            }

            foreach (var emailAddress in emailAddressesCopy)
            {
                result[emailAddress] = SignStatus.NonExistent;
            }

            return result;
        }
    }
}
