﻿using CLAM.Models.GitLab.Webhook;
using System.Threading.Channels;

namespace CLAM.Services.Background
{
    public class GitLabChannel
    {
        public GitLabChannel()
        {
            Queue = Channel.CreateUnbounded<Payload>();
        }

        public ChannelReader<Payload> Reader => Queue.Reader;

        public ChannelWriter<Payload> Writer => Queue.Writer;

        private Channel<Payload> Queue { get; }
    }
}
