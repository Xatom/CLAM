using System.Net.Mail;
using System.Threading.Channels;

namespace CLAM.Services.Background
{
    public class EmailChannel
    {
        public EmailChannel()
        {
            Queue = Channel.CreateUnbounded<MailMessage>();
        }

        public ChannelReader<MailMessage> Reader => Queue.Reader;

        public ChannelWriter<MailMessage> Writer => Queue.Writer;

        private Channel<MailMessage> Queue { get; }
    }
}
