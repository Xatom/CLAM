using CLAM.Options;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace CLAM.Services.Background
{
    public class EmailBackgroundService : BackgroundService, IDisposable
    {
        public EmailBackgroundService(IOptions<SmtpOptions> options, EmailChannel channel, ILogger<EmailBackgroundService> logger)
        {
            Client = new SmtpClient(options.Value.Host, options.Value.Port);
            Reader = channel.Reader;
            Logger = logger;

            if (!string.IsNullOrEmpty(options.Value.User) && !string.IsNullOrEmpty(options.Value.Pass))
            {
                Client.Credentials = new NetworkCredential(options.Value.User, options.Value.Pass);
            }
        }

        private SmtpClient Client { get; }

        private ChannelReader<MailMessage> Reader { get; }

        private ILogger<EmailBackgroundService> Logger { get; }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var message = await Reader.ReadAsync(stoppingToken);
                    await Client.SendMailAsync(message);
                }
                catch (Exception exception)
                {
                    Logger.LogWarning(exception, "Failed to send email");
                }
            }
        }

        public override void Dispose()
        {
            Client.Dispose();
            base.Dispose();
        }
    }
}
