﻿using CLAM.Models.GitLab.Webhook;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace CLAM.Services.Background
{
    public class GitLabBackgroundService : BackgroundService, IDisposable
    {
        public GitLabBackgroundService(IServiceScopeFactory serviceScopeFactory, GitLabChannel channel, ILogger<GitLabBackgroundService> logger)
        {
            ServiceScope = serviceScopeFactory.CreateScope();
            Reader = channel.Reader;
            Logger = logger;
        }

        private IServiceScope ServiceScope { get; }

        private ChannelReader<Payload> Reader { get; }

        private ILogger<GitLabBackgroundService> Logger { get; }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var gitLabService = ServiceScope.ServiceProvider.GetService<GitLabService>();

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var payload = await Reader.ReadAsync();
                    await gitLabService.CheckMergeRequest(payload.Project.Id, payload.ObjectAttributes.Iid, payload.ClaId);
                }
                catch (Exception exception)
                {
                    Logger.LogWarning(exception, "Failed to handle webhook");
                }
            }
        }

        public override void Dispose()
        {
            ServiceScope.Dispose();
        }
    }
}
