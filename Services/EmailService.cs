using CLAM.Models.Email.Account;
using CLAM.Options;
using CLAM.Services.Background;
using Microsoft.Extensions.Options;
using RazorLight;
using System.Net.Mail;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace CLAM.Services
{
    public class EmailService
    {
        public EmailService(IOptions<SmtpOptions> options, RazorLightEngine razorLightEngine, EmailChannel channel)
        {
            Options = options;
            RazorLightEngine = razorLightEngine;
            Writer = channel.Writer;
        }

        private IOptions<SmtpOptions> Options { get; }

        private RazorLightEngine RazorLightEngine { get; }

        private ChannelWriter<MailMessage> Writer { get; }

        public async Task Verify(string email, string verifyUrl)
        {
            var body = await RazorLightEngine.CompileRenderAsync("Account/Verify.cshtml", new Verify
            {
                Email = email,
                Url = verifyUrl
            });

            var message = new MailMessage(Options.Value.From, email, "Verify", body)
            {
                BodyEncoding = Encoding.UTF8,
                SubjectEncoding = Encoding.UTF8
            };

            await Writer.WriteAsync(message);
        }
    }
}
