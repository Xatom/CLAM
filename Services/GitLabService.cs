﻿using CLAM.Models.GitLab.Api;
using CLAM.Models.GitLab.Webhook;
using CLAM.Models.Services.Signer;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CLAM.Services
{
    public class GitLabService
    {
        public GitLabService(HttpClient client, ClaService claService, SignerService signerService)
        {
            Client = client;
            ClaService = claService;
            SignerService = signerService;
        }

        private HttpClient Client { get; }

        private ClaService ClaService { get; }

        private SignerService SignerService { get; }

        public async Task CheckMergeRequest(int projectId, int mergeRequestIdd, int claId)
        {
            var apiKey = await ClaService.GetApiKey(claId);
            var commits = await ListCommits(apiKey, projectId, mergeRequestIdd);
            var emailAddresses = commits.Select(c => c.AuthorEmail).ToHashSet();
            var signStatuses = await SignerService.SignStatuses(claId, emailAddresses);

            await SetLabel(projectId, signStatuses.All(s => s.Value == SignStatus.Signed));
            var toSend = signStatuses.Where(s => s.Value == SignStatus.NonExistent).Select(s => s.Key).ToHashSet();

            if (toSend.Any())
            {
                await SendEmails(toSend);
            }
        }

        public async Task<List<Commit>> ListCommits(string apiKey, int projectId, int mergeRequestIid)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"projects/{projectId}/merge_requests/{mergeRequestIid}/commits");
            request.Headers.Add("Private-Token", apiKey);
            var response = await Client.SendAsync(request);

            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Commit>>(json);
        }

        public Task SetLabel(int projectId, bool signed)
        {
            // TODO: handle label
            return Task.CompletedTask;
        }

        public Task SendEmails(ISet<string> emailAddresses)
        {
            // TODO: handle send emails
            return Task.CompletedTask;
        }
    }
}
