﻿using CLAM.Data;
using CLAM.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CLAM.Services
{
    public class ClaService
    {
        public ClaService(Database database)
        {
            Database = database;
        }

        private Database Database { get; }

        public async Task<List<Cla>> List(string email)
        {
            return await Database.Clas.Where(g => g.UserClas.Any(u => u.User.Email == email)).ToListAsync();
        }

        public async Task<Cla> Single(string email, int id)
        {
            return await Database.Clas.FirstOrDefaultAsync(g => g.Id == id && g.UserClas.Any(u => u.User.Email == email));
        }

        public async Task<bool> Any(string email, int id)
        {
            return await Database.Clas.AnyAsync(g => g.Id == id && g.UserClas.Any(u => u.User.Email == email));
        }

        public async Task<bool> Create(string email, string name, string apiKey)
        {
            var user = await Database.Users.FirstOrDefaultAsync(u => u.Email == email);

            if (user == null)
            {
                return false;
            }

            var secret = Guid.NewGuid();
            var cla = new Cla
            {
                Name = name,
                ApiKey = apiKey,
                Secret = $"{secret}"
            };

            var userCla = new UserCla
            {
                User = user,
                Cla = cla
            };

            Database.Clas.Add(cla);
            Database.UserClas.Add(userCla);
            await Database.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Edit(string email, int id, string name, string apiKey)
        {
            var cla = await Single(email, id);

            if (cla == null)
            {
                return false;
            }

            cla.Name = name;
            cla.ApiKey = apiKey;

            Database.Clas.Update(cla);
            await Database.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ValidateSecret(int id, string secret)
        {
            return await Database.Clas.AnyAsync(p => p.Id == id && p.Secret == secret);
        }

        public async Task<string> GetApiKey(int id)
        {
            return await Database.Clas.Where(p => p.Id == id).Select(p => p.ApiKey).FirstOrDefaultAsync();
        }
    }
}
